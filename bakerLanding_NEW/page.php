<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<!-- test custom post type -->

<div class="head" style="background-image:url(<?php the_field('big_slide_image');?>);">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="<?php echo get_bloginfo('template_directory');?>/slices/Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    
    <div id="phone">
      <p><span id="">Toll-Free:</span> <a href="tel:855-487-7888">855-487-7888</a></p>
      <a href="tel:855-487-7888" class="phone_icon"></a>
    </div><!-- close phone-->

    <div id="SlideInfo">

	<div class="VertAlign">
      	<h1><?php the_field('headline'); ?></h1>
		
		<?php if(get_field('play_button_url_')): ?>
			<a href="<?php the_field('play_button_url_'); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/slices/Video Ply BTN.png" alt="Play Video"></a>
		<?php endif; ?>

	<p><?php the_field('sub-headline') ?></p>
	</div>

    </div><!-- close SlideInfo -->

  </header>
</div><!-- close head -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
