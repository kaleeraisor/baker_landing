<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after
 */
?>
<?php the_field('extratracking_code'); ?>


<script>
$('input').jvFloat();

$(document).ready(function(){
  if(isMobile.any()){
    console.log('Mobile');
      if($(window).width() < 767){
        console.log('Width Restrict');
          $('.programs_row .hold h3').click(function(){
              $(this).toggleClass('open');
              $(this).next('section').toggle();
              console.log('toggle');
          })
      }
  }
});


//Dynamic Header height 
    //Adjust element dimensions on resize event
        if($(window).width() > 768){
          $(window).on('resize', function () {
              $('.head').css('height', $(this).height() + "px");

              var rightSpace = $(window).width() - ($('.content_upper .content').offset().left + $('.content_upper .content').outerWidth());
              var banHeight = $(window).height() - 65 ;
              
          });
        }
        else{
          $('.head').css('height', $(this).height() + "px");
        }
    

    //Trigger the event
    $(window).trigger('resize');


//Follow form Js
$(window).on('resize', function () {
    formScroll();
});




function formScroll() {
  var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
  var fixmePosition = $('.fixme').position();
  var leftmargin = $('.fixme').css('margin-left');
  //var width = document.getElementById('fix').offsetWidth;
  //var intoSize = $('.intro').height() + 35;
  var intoSize = $('#intro').height();
  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
console.log(collapsePoint);

  //$(window).scroll(function() {   
  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
        var currentScroll = $(window).scrollTop();        // get current position  
        var windowSize = $(window).width();
        var formHeight= $('.fixme').height();
        /*var tilesHeight= $('.tiles').offset().top ;
        var stopMe = tilesHeight - formHeight;*/

       
        if (currentScroll >= (fixmeTop - 100) && windowSize > 1000) {    // apply position: fixed if you
          
            var fromTop = $('.fixme').offset().top;
            $('.fixme').css({                                 // scroll to that element or below it
          position: 'fixed',
          top: '80px',
          left: fixmePosition.left  + 'px',
          marginTop: '-' + intoSize + 'px',
          });
        } 
        else {                               // apply position: static
        $('.fixme').css({                      // if you scroll above it
          position:'relative',
          top:'auto',
          left :'auto',
            marginTop: '-' + intoSize + 'px',
          });
        }

        if (currentScroll >= collapsePoint && windowSize > 1000) {   
            $('.form_holder form.keypath').slideUp();
        }
        if (currentScroll < collapsePoint && windowSize > 1000) {   
            $('.form_holder form.keypath').slideDown();
        }
        /*if (currentScroll >= (stopMe - 100) && windowSize > 1000) {           // apply position: fixed if you
              $('.fixme').css({                                 // scroll to that element or below it
                  position: 'absolute',
                  width: width + 'px',
                  top: stopMe + 'px',
                  left: fixmePosition.left + 'px',
                    marginTop: '-' + intoSize + 'px',
                });
        }*/
  });
console.log (leftmargin);
}


$(document).ready(function(){
    
    formScroll();
    $('#Intro').click(function(){
      $('form.keypath').slideToggle();
      var formState = $("form.keypath");
      var windowSize = $(window).width();

    })

});



function titleAlign(){
  var getHeight = $(window).height();
  var getWidth = $(window).width();
  var logoHeight = $('header #logo').height();
  var topSpace = logoHeight + 30;
  if( getHeight <= 360 && getWidth < 765 ){
     $('.mobileForm').css('margin-top','0px');
     $('header #SlideInfo .VertAlign').css('padding-top','90px');
  }

  else{
    var titlePad = getHeight/2 - 120;
    $('header #SlideInfo .VertAlign').css('padding-top', titlePad + 'px');
    $('.mobileForm').css('margin-top','-85px');
  }
}

function formSwap(){
  var getWidth = $(window).width();
  if(getWidth <= 900){
    $('.mobileForm form.keypathMobile').remove();
    $('div.aside .keypath').detach().insertAfter('.mobileForm #Intro');           
  }
  else
  {
    $('.mobileForm .keypath').detach().insertAfter('div.aside #Intro'); 
  }
}
</script>

<script>
$(function(){
  var mobileTop = $('form.keypath').offset().top;
	var mobileHeight = $('form.keypath').height();
	var windowSize = $(window).width();
	var ShowHeightMobile = mobileTop + mobileHeight
  $(window).scroll(function(){
	   var currentScroll = $(window).scrollTop()
	    if (currentScroll > ShowHeightMobile && windowSize < 1000 ) {
        $('.goToFormMobile').css('display', 'block');
		    $('.goToFormMobile').addClass("slideUp");
    	} else {
        $('.goToFormMobile').css('display', 'none');
    		}
        });
  });
</script>

<script>

/* Mobile Keyboard On Detection for Avoid title text merging with request info form */

// Detect Mobile Device
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}; 

$(document).ready(function(){
  
  if(isMobile.any()){

    var _originalSize = $(window).width() + $(window).height();
    var _originalHeadHeight = $(window).height();

      $(window).resize(function(){
        if($(window).width() + $(window).height() != _originalSize){
          console.log("keyboard on");
          //alert('keyboard on'); 
          $(".head").css("height", _originalHeadHeight + "px");  
        }else{
          console.log("keyboard off");
          //alert('keyboard off');
        }
      });
  }

     
  

});
</script>
<?php wp_footer(); ?>

</body>

</html>
